#!/usr/bin/python3

import numpy as np
import gzip
import csv

import tqdm
import tqlog

import argparse
import logging


def parse_np(lines):
    for line in lines:
        try:
            v = np.asarray(line, dtype=np.float32).reshape(1, -1)
        except ValueError:
            logging.info("Bad line: %s", line)
            continue
        yield v

def chunker(lines, batch_size=0x20000):
    accumulator = list()
    for line in lines:
        accumulator.append(line)
        if len(accumulator) >= batch_size:
            yield np.concatenate(accumulator)
            accumulator = list()
    if len(accumulator) > 0:
        yield np.concatenate(accumulator)


def parse_args():
    parser = argparse.ArgumentParser(description='convert.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    parser.add_argument('--chunk', dest='chunk_size', default=0x20000, type=int, required=False)
    args = parser.parse_args()
    return args

def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[tqlog.TqdmLoggingHandler()])

    chunk = args.chunk_size

    logging.debug("Loading dataset %s", args.input)
    with gzip.open(args.input, 'rt', newline='') as fr:
        reader = csv.reader(fr)
        chunks = list(chunker(parse_np(tqdm.tqdm(reader)), batch_size=chunk))
        logging.debug("Chunks %d", len(chunks))
        data = np.concatenate(chunks)
    logging.debug("Shape %s", data.shape)

    d = {'label': data[:,0:1], 'values': data[:,1:28]}
    if data.shape[1] > 28:
        d['mass'] = data[:,28:29]

    logging.debug("Writing file %s", args.output)
    np.savez(args.output, **d)
    logging.debug("Done")

if __name__ == '__main__':
    main()
