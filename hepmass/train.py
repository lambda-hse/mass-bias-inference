import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data
import tqdm
import tqlog
import sklearn.metrics

import numpy as np

import model as m
import loader

import argparse
import logging

@torch.no_grad()
def validate(model, val_dl, prefix='Val'):
    model.train(False)
    preds = list()
    true = list()
    masses = list()
    loss = nn.BCEWithLogitsLoss()
    with tqdm.tqdm(leave=False, disable=None, desc=prefix, total=len(val_dl.dataset)) as pbar:
        val_loss = 0
        val_count = 0
        for batch in val_dl:
            label, mass, values = batch
            ones = torch.ones_like(label)
            #p0 = model(values, mass=mass, label=-ones)
            #p1 = model(values, mass=mass, label=ones)
            #prediction = p1-p0
            prediction = model(values, mass=mass)
            loss_ = loss(prediction, label)
            preds.append(prediction)
            true.append(label)
            masses.append(mass)
            val_loss += loss_.item() * label.shape[0]
            val_count += label.shape[0]
            pbar.set_postfix(loss=val_loss/val_count)
            pbar.update(label.shape[0])
    p = torch.vstack(preds)
    y = torch.vstack(true)
    m = torch.vstack(masses)
    logging.info("loss %f", loss(p, y).item())
    logging.info("ROC AUC: %f", sklearn.metrics.roc_auc_score(y_true=y.cpu(), y_score=p.cpu()))
    mi = (m+0.5).int()
    uniq = mi.unique().cpu().numpy()
    for mass in uniq:
        idx = (mi == mass)
        logging.info("%d loss %f", mass, loss(p[idx], y[idx]).item())
        logging.info("%d ROC AUC: %f", mass, sklearn.metrics.roc_auc_score(y_true=y[idx].cpu(), y_score=p[idx].cpu()))


def train_epoch(model, dl, optimizer, prefix='Train'):
    loss = nn.BCEWithLogitsLoss()
    with tqdm.tqdm(leave=False, disable=None, desc=prefix, total=len(dl.dataset)) as pbar:
        model.train(True)
        train_loss = 0
        train_count = 0
        for batch in dl:
            label, mass, values = batch
            ones = torch.ones_like(label)
            #p0 = model(values, mass=mass, label=-ones)
            #p1 = model(values, mass=mass, label=ones)
            #prediction = p1-p0
            prediction = model(values, mass=mass)
            loss_ = loss(prediction, label)
            train_loss += loss_.item() * label.shape[0]
            train_count += label.shape[0]

            optimizer.zero_grad()
            loss_.backward()
            optimizer.step()

            pbar.set_postfix(loss=train_loss/train_count)
            pbar.update(label.shape[0])
        logging.info("train loss %f", train_loss/train_count)
    

def train(args, trainset, valset=None):
    device = args.device
    model = m.model().to(device)
    params = model.parameters()
    optimizer = torch.optim.Adam(params)

    dl = loader.wrap_sub(trainset, shuffle=True, batch_size=args.batch_size)
    val_dl = loader.wrap_sub(valset, shuffle=False, batch_size=args.batch_size)

    for epoch in tqdm.tqdm(range(args.epochs), disable=None, desc='Epoch'):
        train_epoch(model, dl, optimizer)
        validate(model, val_dl)

        name = "model-%03d" % epoch
        logging.debug("Checkpoint %s", name)
        torch.save(model.state_dict(), name)


def parse_args():
    default_device = "cuda:0" if torch.cuda.is_available() else "cpu"
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--device', dest='device', help='GPU device id to use [%s]' % default_device, default=default_device, type=str)
    parser.add_argument('--batch-size', dest='batch_size', default=8192, type=int)
    parser.add_argument('--epochs', dest='epochs', default=100, type=int)
    parser.add_argument('--dataset', dest='dataset', default=None, type=str, required=True)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[tqlog.TqdmLoggingHandler()])

    logging.debug("Loading dataset %s", args.dataset)
    with np.load(args.dataset, mmap_mode='r') as f:
        v = [torch.as_tensor(f[k], device=args.device, dtype=torch.float) for k in ['label', 'mass', 'values']]
        full_dataset = torch.utils.data.TensorDataset(*v)
    logging.debug("Full dataset size %d", len(full_dataset))
    train_size = len(full_dataset)*4 // 5
    val_size = len(full_dataset) - train_size
    train_dataset, val_dataset = torch.utils.data.random_split(full_dataset, [train_size, val_size])
    logging.debug("Train dataset size %d", len(train_dataset))
    logging.debug("Validation dataset size %d", len(val_dataset))

    train(args, train_dataset, val_dataset)

if __name__ == '__main__':
    main()
