import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data
import tqdm
import tqlog

import numpy as np

import model as m
import loader

import argparse
import logging



class Model(nn.Module):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.mu = nn.Parameter(torch.zeros(1)+0.5)
        self.lsigma = nn.Parameter(torch.zeros(1))
    def forward(self, z):
        v = self.mu + z * torch.exp(self.lsigma)
        return torch.sigmoid(v)*(2000-250) + 250

def infer(args, dataset):
    device = args.device
    model = m.model().to(device)
    loss = nn.BCEWithLogitsLoss()

    m2 = Model().to(device)
    params = m2.parameters()
    optimizer = torch.optim.Adam(params)

    logging.debug("Loading checkpoint %s", args.ckpt)
    model.load_state_dict(torch.load(args.ckpt, map_location=torch.device(device)))
    model.eval()

    dl = loader.wrap(dataset, shuffle=True, batch_size=args.batch_size)

    for epoch in tqdm.tqdm(range(args.epochs), disable=None, desc='Epoch'):
        with tqdm.tqdm(leave=False, disable=None, desc='Infer', total=len(dataset)) as pbar:
            m2.train(True)
            test_loss = 0
            test_count = 0
            for batch in dl:
                label, values = batch
                r = torch.randn_like(label)
                mass = m2(r)
                prediction = model(values, mass) #.flatten()
                loss_ = loss(prediction, label)

                optimizer.zero_grad()
                loss_.backward()
                optimizer.step()

                test_loss += loss_.item() * label.shape[0]
                test_count += label.shape[0]
                with torch.no_grad():
                    pbar.set_postfix(loss=test_loss/test_count, mass=m2.forward(0).item(), sigma=torch.exp(m2.lsigma).item())
                pbar.update(label.shape[0])
            test_loss /= len(dataset)
        sigma = torch.exp(m2.lsigma).item()
        with torch.no_grad():
            v = m2.forward(torch.tensor([0.0,-1.0,1.0], device=device)).cpu().numpy()
        logging.info("epoch %d mass %f+%f-%f sigma %f", epoch, 
                v[0], v[2]-v[0], v[0]-v[1],
                sigma)


def parse_args():
    default_device = "cuda:0" if torch.cuda.is_available() else "cpu"
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--device', dest='device', help='GPU device id to use [%s]' % default_device, default=default_device, type=str)
    parser.add_argument('--batch-size', dest='batch_size', default=8192, type=int)
    parser.add_argument('--epochs', dest='epochs', default=50, type=int)
    parser.add_argument('--dataset', dest='dataset', default=None, type=str, required=True)
    parser.add_argument('--checkpoint', dest='ckpt', default=None, type=str, required=True)
    parser.add_argument('--mass', dest='mass', default=None, type=int)
    parser.add_argument('--label', dest='label', default=None, type=int)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[tqlog.TqdmLoggingHandler()])

    logging.debug("Loading dataset %s", args.dataset)
    with np.load(args.dataset, mmap_mode='r') as f:
        v = [torch.as_tensor(f[k], device=args.device, dtype=torch.float) for k in ['label', 'values']]
        if args.mass is not None:
            m = torch.as_tensor(f["mass"], device=args.device, dtype=torch.float)
            mi = (m+0.5).int().flatten()
            idx = (mi == args.mass)
            v = [x[idx] for x in v]
        if args.label is not None:
            mi = (v[0]+0.5).int().flatten()
            idx = (mi == args.label)
            v = [x[idx] for x in v]
        dataset = torch.utils.data.TensorDataset(*v)

    logging.debug("Dataset size %d", len(dataset))

    infer(args, dataset)

if __name__ == '__main__':
    main()
