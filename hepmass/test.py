import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data
import tqdm
import tqlog
import torchsummary

import numpy as np

import model as m
import loader

import argparse
import logging

import train

@torch.no_grad()
def test(args, testset):
    device = torch.device(args.device)
    model = m.model()
    model.eval()
    logging.debug(torchsummary.summary(model.to('cpu'), (27,), device='cpu'))
    model.to(device)

    logging.debug("Loading checkpoint %s", args.ckpt)
    model.load_state_dict(torch.load(args.ckpt, map_location=torch.device(device)))

    test_dl = loader.wrap(testset, shuffle=False, batch_size=args.batch_size)
    train.validate(model, test_dl, prefix='Test')

def parse_args():
    default_device = "cuda:0" if torch.cuda.is_available() else "cpu"
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--device', dest='device', help='GPU device id to use [%s]' % default_device, default=default_device, type=str)
    parser.add_argument('--batch-size', dest='batch_size', default=8192, type=int)
    parser.add_argument('--dataset', dest='dataset', default=None, type=str, required=True)
    parser.add_argument('--checkpoint', dest='ckpt', default=None, type=str, required=True)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[tqlog.TqdmLoggingHandler()])

    logging.debug("Loading dataset %s", args.dataset)
    with np.load(args.dataset, mmap_mode='r') as f:
        v = [torch.as_tensor(f[k], device=args.device, dtype=torch.float) for k in ['label', 'mass', 'values']]
        dataset = torch.utils.data.TensorDataset(*v)
    logging.debug("Dataset size %d", len(dataset))
    test(args, dataset)

if __name__ == '__main__':
    main()
