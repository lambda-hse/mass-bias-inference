import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

def linear_init(*args, **kwargs):
    linear = nn.Linear(*args, **kwargs)
    linear.weight.data.normal_(0.0, np.sqrt(1/linear.in_features))
    return linear

def linear_selu(*args, **kwargs):
    return nn.Sequential(linear_init(*args, **kwargs), nn.SELU())

def full(n, hid, **kwargs):
    layers = [linear_selu(hid, hid) for _ in range(n)]
    return nn.Sequential(*layers)

class Model(nn.Module):
    def __init__(self, *args, outs=1, layers=5, hid=100, **kwargs):
        super().__init__(*args, **kwargs)
        self.linear1 = nn.Linear(27, hid)
        self.linear2 = nn.Linear(1, hid, bias=False)
        self.linear3 = nn.Linear(1, hid, bias=False)
        self.mid = full(layers, hid, bias=False)
        self.other = linear_init(hid, outs)
        self.linear1.weight.data.normal_(0.0, np.sqrt(1/29))
        self.linear2.weight.data.normal_(0.0, np.sqrt(1/29))
        self.linear3.weight.data.normal_(0.0, np.sqrt(1/29))

    def forward(self, values, mass=None, label=None):
        s = self.linear1(values)
        if mass is not None:
            s += self.linear2((mass - 1000)/500)
        if label is not None:
            s += self.linear3(label)
        #return s
        mid = self.mid(F.selu(s))
        return self.other(mid)

def model(*args, **kwargs):
    return Model(*args, **kwargs)
