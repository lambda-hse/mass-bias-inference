import torch.utils.data.dataset

class Array(torch.utils.data.dataset.Dataset):
    def __init__(self, *datasets):
        super().init()
        self.length = None
        for dataset in datasets:
            length = dataset.shape[0]
            if self.length is None:
                self.length = length
            else:
                if length != self.length:
                    raise ValueError("Array shapes do not match")
        self.datasets = datasets

    def __getitem__(self, idx):
        return [torch.as_tensor(dataset[idx]) for dataset in self.datasets]

    def __len__(self):
        return self.length

class TensorDict(torch.utils.data.dataset.TensorDataset):
    def __init__(self, **datasets):
        super().__init__(*datasets.values())
        self.keys = datasets.keys()

    def __getitem__(self, idx):
        v = super().__getitem__(idx)
        return dict(zip(self.keys, v))

    def to(self, device):
        v = [t.to(device) for t in self.tensors]
        return TensorDict(**dict(zip(self.keys, v)))


def wrap(data, *args, **kwargs):
    r = data
    t = None
    if type(data) is torch.utils.data.dataset.Subset:
        r = data.indices
        t = data.dataset.tensors
    elif type(data) is torch.utils.data.dataset.TensorDataset:
        r = torch.arange(len(data))
        t = data.tensors
    def collate(idx):
        ix = torch.as_tensor(idx)
        return [x[ix] for x in t]
    kwargs['collate_fn'] = collate
    return torch.utils.data.DataLoader(r, *args, **kwargs)

def wrap_sub(data, *args, **kwargs):
    r = data.indices
    def collate(idx):
        return [x[idx] for x in data.dataset.tensors]
    kwargs['collate_fn'] = collate
    return torch.utils.data.DataLoader(r, *args, **kwargs)
