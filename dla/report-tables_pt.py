#!/usr/bin/env python3

import numpy as np
import pandas as pd
import torch

import pickle
import json

import argparse
import logging

import lib.util
import lib.setup
import lib.files


log = logging.getLogger(__name__)
np.set_printoptions(precision=4, suppress=True)


def parse_args():
    default_device = "cuda:0" if torch.cuda.is_available() else "cpu"
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--device', dest='device', help='GPU device id to use [%s]' % default_device, default=default_device, type=str)
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.INFO, type=str)
    parser.add_argument('--data', dest='data', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=False)
    parser.add_argument('input', type=str, nargs='+')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)
    log.info("device: %s", args.device)


    data_dict = torch.load(args.data)
    data = lib.data.PlainDataset(
            data_dict['features'],
            data_dict['classes'],
            data_dict['X'].to(args.device),
            data_dict['Y'].to(args.device),
            )
    X = data.X()
    Y1 = data.Y_hot().float()

    table = list()
    for name in args.input:
        with open(name, "rb") as f:
            w_history = pickle.load(f)

        rks = w_history.extract("rk")
        best = np.argmax(rks[1])
        frame = rks[0][best]
        log.info("Frame %d", frame)

        m = w_history.measurements[frame]
        model = m.get("model", None).to(args.device)
        features = model[0].features
        if features != data_dict["features"]:
            log.warning("Features do not match for %s (%s!=%s)", name, features, data_dict["features"])
            continue

        rec = {'name': name}

        with torch.no_grad():
            w = model(X)
        yw = (Y1.T@w).cpu().numpy().flatten()
        res = yw/data.counts
        log.info("Res: %s", res)
        for i, c in enumerate(data.classes):
            rec["e(%s)" % c] = 1-res[i]
            rec["R(%s)" % c] = (1-res[i])/(1-res[1])

        table.append(rec)

    df = pd.DataFrame.from_records(table)
    print(df.to_markdown())

    if args.output is not None:
        with open(args.output, 'w') as f:
            json.dump(table, f)

if __name__ == '__main__':
    main()
