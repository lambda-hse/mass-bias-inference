#!/usr/bin/env python3

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation

import numpy as np
import pandas as pd

import pickle

import argparse
import logging

import lib.graph_util
import lib.graph
import lib.util
import lib.setup


log = logging.getLogger(__name__)
np.set_printoptions(precision=4, suppress=True)


def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    parser.add_argument('--dpi', dest='dpi', default=120, type=int, required=False)
    parser.add_argument('--frame', dest='frame', default=None, type=int, required=False)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)

    with open(args.input, "rb") as f:
        w_history = pickle.load(f)

    matplotlib.use('agg')
    plt.ioff()

    rks = w_history.extract("rk")
    rk0s = w_history.extract("rk0")

    frame = args.frame
    if frame is None:
        best = np.argmax(rks[1])
        frame = rks[0][best]
    log.info("Frame %d", frame)

    i = frame
    m = w_history.measurements[frame]

    fig = plt.figure(figsize=(2, 2), facecolor='white')
    ax_model = fig.gca()
    model = m.get("model", None)
    sc = model[0]
    f = lib.util.as_numpy(model)
    lib.graph.plot_field(f, sc, ax=ax_model, color_bar=False)
    fig.savefig(args.output, dpi=args.dpi, transparent=False, bbox_inches='tight')


if __name__ == '__main__':
    main()
