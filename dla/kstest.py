#!/usr/bin/env python3

import numpy as np
import pandas as pd
import scipy.stats

import argparse
import logging
import tqdm

import lib.tqlog
import lib.model
import lib.data
import lib.files


def perform_ks_test(datasets, selected_index):
    selected_dataset = datasets[selected_index]
    df_ks = pd.DataFrame()
    #df_ks.index.name = "Features"
    df_ks.columns.name = "KS test"
    for feature in selected_dataset.features():
        arr0 = selected_dataset.get([feature])[:,0]
        for index, arr in enumerate(datasets):
            if index == selected_index:
                continue
            if not feature in arr.features():
                continue
            arr1 = arr.get([feature])[:,0]
            df_ks.loc[feature, arr.name] = scipy.stats.kstest(arr0,arr1).statistic
    return df_ks


def parse_args():
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    #parser.add_argument('--dataset', dest='dataset', default=None, type=str, required=True)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[lib.tqlog.TqdmLoggingHandler()])

    np.set_printoptions(precision=4, suppress=True)

    sources = lib.files.load(with_hits=False)

    df = perform_ks_test(sources, 0)

    print(df.to_string())


if __name__ == '__main__':
    main()
