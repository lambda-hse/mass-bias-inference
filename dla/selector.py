#!/usr/bin/env python3

import torch
import numpy as np

import argparse
import logging

import pickle

import lib.tqlog
import lib.model
import lib.data
import lib.files
import lib.trainer
import lib.logger


log = logging.getLogger(__name__)



def parse_args():
    default_device = "cuda:0" if torch.cuda.is_available() else "cpu"
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--device', dest='device', help='GPU device id to use [%s]' % default_device, default=default_device, type=str)
    parser.add_argument('--dataset', dest='dataset', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    parser.add_argument('--features', dest='features', default=None, type=str, required=True)
    parser.add_argument('--channels', dest='channels', default=None, type=int, required=False)
    parser.add_argument('--q', dest='q', default=None, type=float, required=False)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[lib.tqlog.TqdmLoggingHandler()])
    log.info("device: %s", args.device)

    np.set_printoptions(precision=4, suppress=True)

    sources = lib.files.load(args.dataset, channels=args.channels)
    truthed = lib.data.truthing(sources, true=True, q=args.q)

    features = args.features.split(',')
    aliases = {
        "min_e_IP_TRUE": "fmin(ep_IP_TRUE,em_IP_TRUE)",
        "max_e_PT_TRUE": "fmax(ep_PT_TRUE,em_PT_TRUE)",
        "max_e_SIGMAIP_TRUE": "fmax(ep_SIGMAIP_TRUE,em_SIGMAIP_TRUE)",
        "max_e_P_TRUE": "fmax(ep_P_TRUE,em_P_TRUE)",

        "min_e_IP": "fmin(ep_IP,em_IP)",
        "max_e_PT": "fmax(ep_PT,em_PT)",
        "max_e_P": "fmax(ep_P,em_P)",
    }
    data = lib.data.Dataset(features, truthed, aliases)

    log.info("Saving data to '%s'", args.output)
    meta = dict()
    meta["features"] = data.features
    meta["classes"] = data.classes
    meta["aliases"] = data.aliases
    meta["name"] = data.name
    meta["X"] = data.X(dtype=torch.float)
    meta["Y"] = data.Y(dtype=torch.int64)
    torch.save(meta, args.output)


if __name__ == '__main__':
    main()
