#!/usr/bin/env python3

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation

import numpy as np
import pandas as pd
import xdrlib

import pickle

import argparse
import logging

import lib.graph_util
import lib.graph
import lib.util
import lib.setup


log = logging.getLogger(__name__)
np.set_printoptions(precision=4, suppress=True)



def make_xdr(f, sc):
    x_min, y_min = sc.min
    x_max, y_max = sc.max

    p1, p2 = sc.transform([sc.min, sc.max])
    c = (p1+p2)/2
    d = np.max((p2-p1)/2)
    q_min, q_max = sc.inverse_transform([c - d, c + d])
    log.warning("dims %s %s", p1, p2)
    log.warning("scale %s %s", c, d)
    log.warning("scale %s %s", q_min, q_max)

    xi = np.linspace(q_min[0], q_max[0], 301)
    yi = np.linspace(q_min[1], q_max[1], 301)
    xy = np.asarray(np.meshgrid(xi, yi)).transpose(2,1,0)

    log.warning("shape xy %s", xy.shape)

    a = 1.0-f(xy)

    log.warning("shape f(xy) %s", a.shape)

    assert a.shape[0] == len(xi)
    assert a.shape[1] == len(yi)

    p = xdrlib.Packer()
    p.pack_int(a.shape[0])
    p.pack_int(a.shape[1])
    p.pack_farray(a.size, a.flatten(), p.pack_double)
    p.pack_double(q_min[0])
    p.pack_double(q_max[0])
    p.pack_double(q_min[1])
    p.pack_double(q_max[1])
    return p.get_buffer()




def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    parser.add_argument('--frame', dest='frame', default=None, type=int, required=False)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)

    with open(args.input, "rb") as f:
        w_history = pickle.load(f)

    matplotlib.use('agg')
    plt.ioff()

    rks = w_history.extract("rk")

    frame = args.frame
    if frame is None:
        best = np.argmax(rks[1])
        frame = rks[0][best]
    log.info("Frame %d", frame)

    i = frame
    m = w_history.measurements[frame]
    model = m.get("model", None)
    sc = model[0]
    fn = lib.util.as_numpy(model)

    with open(args.output, 'wb') as fw:
        data = make_xdr(fn, sc)
        fw.write(data)


if __name__ == '__main__':
    main()
