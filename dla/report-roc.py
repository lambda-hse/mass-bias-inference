#!/usr/bin/env python3

import numpy as np
import pandas as pd

import pickle

import argparse
import logging

import lib.util
import lib.setup
import lib.files

from sklearn.metrics import roc_curve, roc_auc_score

import matplotlib.pyplot as plt


log = logging.getLogger(__name__)
np.set_printoptions(precision=4, suppress=True)


def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    parser.add_argument('--dpi', dest='dpi', default=120, type=int, required=False)
    parser.add_argument('--frame', dest='frame', default=None, type=int, required=False)
    parser.add_argument('--dataset', dest='dataset', default=None, type=str, required=True)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)

    sources = lib.files.load(args.dataset)
    truthed = lib.data.truthing(sources, true=True)

    with open(args.input, "rb") as f:
        w_history = pickle.load(f)

    rks = w_history.extract("rk")

    frame = args.frame
    if frame is None:
        best = np.argmax(rks[1])
        frame = rks[0][best]
    log.info("Step %d", frame)

    m = w_history.measurements[frame]
    model = m.get("model", None)
    sc = model[0]
    f = lib.util.as_numpy(model)
    features = sc.features
    log.info("features: %s", features)

    aliases = {
        "min_e_IP_TRUE": "fmin(ep_IP_TRUE,em_IP_TRUE)",
        "max_e_PT_TRUE": "fmax(ep_PT_TRUE,em_PT_TRUE)",
        "max_e_SIGMAIP_TRUE": "fmax(ep_SIGMAIP_TRUE,em_SIGMAIP_TRUE)",
        "max_e_P_TRUE": "fmax(ep_P_TRUE,em_P_TRUE)",

        "min_e_IP": "fmin(ep_IP,em_IP)",
        "max_e_PT": "fmax(ep_PT,em_PT)",
        "max_e_P": "fmax(ep_P,em_P)",
    }

    fig = plt.figure(figsize=(2, 2), facecolor='white')
    ax = fig.gca()

    ax.set_xlabel(truthed[0].name)

    # select indices
    for true_class in truthed[1:]:
        data = lib.data.Dataset(features,
                [truthed[0], true_class],
                aliases)

        x = data.X()
        y_true = data.Y()
        y_score = 1-f(x)
        fpr, tpr, thr = roc_curve(y_true, y_score, sample_weight=None)
        auc = roc_auc_score(y_true, y_score, sample_weight=None)
        log.info('AUC %f, %s, %s', auc, truthed[0].name, true_class.name)

        ax.plot(fpr, tpr, label=true_class.name)

    ax.legend()
    ax.grid()
    ax.set_aspect('equal')
    ax.set_xlim(0.0, 1.0)
    ax.set_ylim(0.0, 1.0)

    fig.savefig(args.output, dpi=args.dpi, transparent=False, bbox_inches='tight')


if __name__ == '__main__':
    main()
