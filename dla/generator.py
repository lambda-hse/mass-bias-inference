#!/usr/bin/env python3

import torch
import numpy as np

import argparse
import logging

import pickle

import sklearn.datasets

import lib.tqlog
import lib.logger


log = logging.getLogger(__name__)



def parse_args():
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[lib.tqlog.TqdmLoggingHandler()])

    np.set_printoptions(precision=4, suppress=True)

    x, y = sklearn.datasets.make_moons(1000000, noise=0.2)

    log.info("Saving data to '%s'", args.output)
    meta = dict()
    meta["features"] = ["x0", "x1"]
    meta["classes"] = ["c0", "c1"]
    meta["aliases"] = {}
    meta["name"] = "moons"
    meta["X"] = torch.tensor(x, dtype=torch.float)
    meta["Y"] = torch.tensor(y, dtype=torch.int64)
    torch.save(meta, args.output)


if __name__ == '__main__':
    main()
