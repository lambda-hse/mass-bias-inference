from . import tqlog
import logging as logging_module

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

def logging(level=logging_module.NOTSET, dest=None):
    log_handlers = [tqlog.TqdmLoggingHandler()]
    if dest is not None:
        fh = logging_module.FileHandler(dest)
        log_handlers.append(fh)
    logging_module.basicConfig(
            level=level,
            format=FORMAT,
            force=True,
            handlers=log_handlers,
            )
