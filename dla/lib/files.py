import os.path
import logging

log = logging.getLogger(__name__)

from . import data

DATASETS = {
        'nohits': [
            "RapidSim/B2Kee_tree.root:DecayTree;29",
            "RapidSim/B2KJpsi2ee_tree.root:DecayTree;36",
            "RapidSim/B2Kpsi2s2ee_tree.root:DecayTree;37",
            "RapidSim/D2phipi2ee_tree.root:DecayTree;31",
            ],
        'hits': [
            "RapidSim/B2Kee_tree-2Dhits.root:DecayTree;27",
            "RapidSim/B2KJpsi2ee_tree-2Dhits.root:DecayTree;34",
            "RapidSim/B2Kpsi2s2ee_tree-2Dhits.root:DecayTree;35",
            "RapidSim/D2phipi2ee_tree-2Dhits.root:DecayTree;30",
            ],
        'big': [
            "RapidSim/B2Kee_tree_presel.root:DecayTree;180",
            "RapidSim/B2KJpsi2ee_tree.root:DecayTree;36",
            "RapidSim/B2Kpsi2s2ee_tree.root:DecayTree;37",
            "RapidSim/D2phipi2ee_tree.root:DecayTree;31",
            ],
        }

CHANNELS = [
    r"B⁺→K⁺ee",
    r"B⁺→K⁺J/ψ(→ee)",
    r"B⁺→K⁺ψ(2S)(→ee)",
    r"D⁺→π⁺φ(→ee)",
]

def load(dataset, channels=None):
    basedir = os.path.expanduser("~/2022/")
    names = DATASETS[dataset]
    if channels is not None:
        names = names[:channels]
    files = [
            data.DataFile(os.path.join(basedir, path), name)
            for path, name in zip(names, CHANNELS)
            ]

    if len(files) >= 4:
        files[3].aliases = {
            'B_FD': "D_FD",
            'B_FD_TRUE': "D_FD_TRUE",
        }

    return files
