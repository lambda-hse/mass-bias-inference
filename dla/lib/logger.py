class Logger:
    def __init__(self, name=None):
        self.measurements = [{}]
        self.name = name

    def last(self):
        return self.measurements[-1]

    def log(self, **kwargs):
        self.measurements.append(kwargs)

    def update(self, **kwargs):
        self.last().update(kwargs)

    def extract(self, key, default=None, skip=True):
        index = list()
        result = list()
        for i, entry in enumerate(self.measurements):
            value = entry.get(key, default)
            if not (skip and value is None):
                index.append(i)
                result.append(value)
        return index, result

