import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as D

import sklearn.preprocessing

from . import util
from . import trainer


def model(inputs, outputs, hid=20):
    return nn.Sequential(
        nn.Linear(inputs, hid), nn.Softplus(),
        nn.Linear(hid, hid), nn.Softplus(),
        nn.Linear(hid, hid), nn.Softplus(),
        nn.Linear(hid, outputs),
    )

class Scaler(torch.nn.Module):
    def __init__(self, dataset, device=None):
        super().__init__()
        self.features = dataset.features
        self.scaler = sklearn.preprocessing.StandardScaler()
        self.minmax = sklearn.preprocessing.MinMaxScaler()
        x = util.to_numpy(dataset.X())
        self.scaler.fit(x)
        self.minmax.fit(x)
        mu = torch.as_tensor(self.scaler.mean_, dtype=float, device=device)
        sigma = torch.as_tensor(self.scaler.scale_, dtype=float, device=device)
        self.min = self.minmax.data_min_
        self.max = self.minmax.data_max_
        self.register_buffer('mu', mu)
        self.register_buffer('sigma', sigma)

    def forward(self, data):
        return ((data-self.mu)/self.sigma).float()

    def transform(self, x):
        return self.scaler.transform(x)

    def inverse_transform(self, x):
        return self.scaler.inverse_transform(x)

class Weighting(torch.nn.Module):
    def __init__(self, scaler, model, device=None, n_outs=None, name=None):
        super().__init__()

        scaler_ = util.clone_model(scaler)
        model_ = util.clone_model(model)

        n_inputs = len(scaler_.features)
        if n_outs is None:
            with torch.no_grad():
                z = torch.zeros(1, n_inputs, dtype=torch.float, device=device)
                n_outs = model_.to(device)(z).shape[1]

        self.features = scaler_.features
        self.n_inputs = n_inputs
        self.n_outs = n_outs
        self.name = name

        self.model = torch.nn.Sequential(
            scaler_,
            model_,
            torch.nn.Softmax(dim=-1),
            torch.nn.Linear(n_outs, 1, bias=False),
        ).to(device).eval()

    def forward(self, x):
        return self.model(x)

    def train(self, dataset, n_classes=None, selected_class=0, logger=None, **kwargs):
        if n_classes is None:
            n_classes = len(dataset.sources)
        return trainer.train_weighting(self.model, dataset, self.n_outs, n_classes, selected_class, logger=logger, **kwargs)

    def fix_linear(self, dataset, selected_class=0):
        loader = Loader(dataset, batch_size=100000, shuffle=False, device=device)
        fix_linear(self.model, loader, self.n_outs, self.n_classes, selected_class)

def make_estimator(dataset, device=None, **kwargs):
    scaler = Scaler(dataset, device=device)
    m = model(len(dataset.features), len(dataset.sources), **kwargs)
    return torch.nn.Sequential(scaler, m).to(device)

def w_from_est(estimator, name=None):
    return Weighting(estimator[0], estimator[1:], name=name)
