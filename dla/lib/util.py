import numpy as np
import torch
import pickle

def to_numpy(t):
    return t.detach().cpu().numpy()

def to_numpy_(*t_):
    return (to_numpy(t) for t in t_)

def from_numpy(t, dtype=torch.float, device=None):
    return torch.as_tensor(t, dtype=dtype, device=device)

def from_numpy_(*t_, dtype=torch.float, device=None):
    return (from_numpy(t, dtype=dtype, device=device) for t in t_)

def as_numpy(f, dtype=torch.float, device=None):
    return lambda *x: to_numpy(f(*from_numpy_(*x, dtype=dtype, device=device)))

def as_numpy_(f, dtype=torch.float, device=None):
    return lambda *x: to_numpy_(*f(*from_numpy_(*x, dtype=dtype, device=device)))

class Lambda(torch.nn.Module):
    def __init__(self, fn):
        super().__init__()
        self.fn = fn
    def forward(self, x):
        return self.fn(x)

def clone_model(model, force_eval=True, set_to_none=True):
    if force_eval:
        model.eval()
    model.zero_grad(set_to_none=set_to_none)
    return pickle.loads(pickle.dumps(model))
