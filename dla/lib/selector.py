
def join_and(*conditions):
    return "&".join(["(%s)"%condition for condition in conditions])

def q2_range(v1, v2, true=True):
    feature = "q2_TRUE" if true else "q2"
    return join_and("%s > %g" % (feature, v1), "%s < %g" % (feature, v2))

def e_selectors(true=True):
    q_Kll_selector = q2_range(1.1, 6.0, true)
    q_Jpsi_e_selector = q2_range(6.0, 12.96, true)
    q_psi2S_e_selector = q2_range(9.82, 16.40, true)
    selectors = [
        q_Kll_selector,
        q_Jpsi_e_selector,
        q_psi2S_e_selector,
        "True",
    ]
    return selectors

selectors_true = e_selectors(True)
selectors_notrue = e_selectors(False)

selected_true = [SelectedData(f, f.get([selector])[:,0]) for f, selector in zip(files, selectors_true)]
selected_notrue = [SelectedData(f, f.get([selector])[:,0]) for f, selector in zip(files, selectors_notrue)]

