import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.distributions as D

import scipy.optimize

import numpy as np

from .loader import Loader
from . import util

import logging

log = logging.getLogger(__name__)

BOUNDS = [(0.01, 0.03), (0, 0.07)]

def create_w(model, loader, n_outs, n_classes, device=None):
    totals = torch.zeros(n_classes, dtype=torch.long, device=device)
    wsum = torch.zeros(n_outs, n_classes, dtype=torch.float, device=device)
    for batch in loader:
        x, y = batch
        y_one_hot = torch.nn.functional.one_hot(y, num_classes=n_classes) # B*n
        pred = model(x) # B*N
        totals += torch.sum(y_one_hot, dim=0) # n
        wsum += torch.sum(pred.view(-1,n_outs,1)*y_one_hot.view(-1,1,n_classes), dim=0) # N*n
    W = wsum/totals.view(1,-1)
    return W, totals.view(1,-1)

def compute_lipschitz(model, loader, device=None):
    model.eval().to(device)
    sc = model[0]
    fn = model[1:]
    L = 0
    for batch in loader:
        x, y = batch
        x.to(device)
        model.zero_grad()
        x_ = sc(x)
        x_.requires_grad = True
        fx = fn(x_)
        loss = torch.sum(fx)
        loss.backward()
        L = max(L, torch.max(x_.grad.norm(dim=1)).item())
    return L

def make_weighting_lp(Wpinv, e_, bounds):
    n_classes, n_outs = Wpinv.shape
    c = np.asarray([-1]+[0]*(n_classes-1))
    bounds_ = ([(0.0, 1.0), (e_, e_)] + bounds)[:n_classes]
    A_ub = np.vstack([Wpinv.T, -Wpinv.T])
    b_ub = np.hstack([np.ones(n_outs), np.zeros(n_outs)])
    res = scipy.optimize.linprog(c=c, bounds=bounds_, A_ub=A_ub, b_ub=b_ub)
    return np.maximum(Wpinv.T@res.x,0) if res.success else None

@torch.no_grad()
def make_weighting(Wpinv, linear, selected_class, n_outs, n_classes, e_, bounds):
    assert Wpinv.shape == (n_classes, n_outs)
    w1 = 1/e_
    w2 = 1/(1-e_)
    weights = make_weighting_lp(Wpinv, e_, bounds)
    if weights is None:
        w0 = Wpinv[selected_class]
        m_minus = np.max(-w0)
        m_plus = np.max(w0)
        m_sum = m_minus + m_plus
        k = np.maximum(m_minus*w1, m_plus*w2)
        weights = (w0 + k*e_)/k
        #weights = (w0 + m_minus)/m_sum
    linear.weight.data = util.from_numpy(weights).view(linear.weight.data.shape)

def fix_linear(model, loader, n_outs, n_classes, selected_class, e_, bounds, device=None):
    with torch.no_grad():
        W, counts = create_w(model[:3], loader, n_outs, n_classes, device=device)
        W_np = util.to_numpy(W)
        W_pinv = np.linalg.pinv(W_np)
        make_weighting(W_pinv, model[3], selected_class, n_outs, n_classes, e_, bounds)
    L = compute_lipschitz(model, loader, device=device)
    log.info("W=%s W+=%s L=%f coef=%s", W_np, W_pinv, L, util.to_numpy(model[3].weight.data))

def rk_from_w(W, e_, bounds):
    Wf = W.astype(np.float64)
    Wp = np.linalg.pinv(Wf)
    res = make_weighting_lp(Wp, e_, bounds)
    w = Wf.T@res
    return (1-w[1])/(1-w[0])

def rk0_from_w(W, selected_class=0, e_=0.02):
    Wf = W.astype(np.float64)
    Wp = np.linalg.pinv(Wf)
    wcj = Wp[selected_class]
    wcj_ = (wcj > 0).astype(np.float64)
    ek_ = e_ + np.min((wcj_-e_)/wcj)
    return (1-e_)/(1-ek_)

def softmax(x):
    m = np.max(x)
    t = np.exp(x-m)
    s = np.sum(t)
    return t/s

def softmin(x):
    return softmax(-x)

def e_w_derivative(W_pinv, selected_class, e_=0.02, temp=10):
    wcj = W_pinv[selected_class]                   # N
    t0 = (wcj>0)-e_
    t1 = t0/wcj
    t1s = softmin(t1*temp)
    wcj_ = -t0/(wcj**2)*t1s
    wki = np.sum(W_pinv*wcj_[None,:], axis=1)      # n
    W_m = np.multiply.outer(wcj, wki)              # N*n ~ W
    return W_m

def e_w_derivative_(W_pinv, selected_class, e_=0.02, temp=10):
    wcj = W_pinv[selected_class]                   # N
    t0 = (wcj>0)-e_
    t1 = t0/wcj
    t1s = softmin(t1*temp)
    #wcj_ = -t0/(wcj**2)*t1s
    wcj_ = ((wcj<0)*(1-e_) - (wcj>1)*e_)*t1s             # f(wcj)
    #ix_min = np.argmin(wcj)
    #ix_max = np.argmax(wcj)
    #wcj_ = np.zeros(n_outs)
    #if m_minus * 49 > m_plus:
    #    wcj_[ix_min] = 1
    #else:
    #    wcj_[ix_max] = -1
    #wcj_[ix_min] = 0.98
    #wcj_[ix_max] = -0.02
    #wcj_ = np.log(1-np.minimum(wcj,0))-np.log(1+np.maximum(wcj-1,0))
    wki = np.sum(W_pinv*wcj_[None,:], axis=1)      # n
    W_m = np.multiply.outer(wcj, wki)              # N*n ~ W
    return W_m

def train_pinv(model, loader, n_outs, n_classes, selected_class, e_=0.02, bounds=BOUNDS, epochs=50, alpha=0.001, dxl=0.001, target_L=None, logger=None, device=None):
    model.to(device)
    clone = util.clone_model(model)
    scaler = model[0]
    meat = model[1:3] # with softmax
    base = model[:3]
    linear = model[3]
    opt = torch.optim.SGD(params=meat.parameters(), lr=1e-4)
    dx_ = torch.tensor([[[-1]],[[1]]], dtype=torch.float, device=device) * dxl
    try:
        for epoch in range(epochs):
            model.eval()
            with torch.no_grad():
                W, counts = create_w(base, loader, n_outs, n_classes, device=device)
            W_np = util.to_numpy(W).astype(np.float64)
            W_pinv = np.linalg.pinv(W_np)
            logdet = np.log(np.linalg.det(W_np.T@W_np))/2
            W_pinv_ = util.from_numpy(W_pinv, device=device)
            W_pinv_T = W_pinv_.T

            # fill linear coeffs
            make_weighting(W_pinv, linear, selected_class, n_outs, n_classes, e_, bounds)

            W_m = e_w_derivative_(W_pinv, selected_class, e_)
            W_m_t = util.from_numpy(W_m, device=device)     # W_m_
            K = 1/np.maximum(1,np.max(np.abs(W_m)))

            for clone_par, model_par in zip(clone.parameters(), model.parameters()):
                clone_par.data.copy_(model_par.data)
            #clone.load_state_dict(model.state_dict())

            if logger is not None:
                logger.update(model=util.clone_model(model).cpu())
                logger.update(logdet=logdet)
                logger.update(rk=rk_from_w(W_np, e_, bounds))
                logger.update(rk0=rk0_from_w(W_np, e_=e_))
                logger.update(K=K)
                logger.update(W=W_np)
                logger.update(Wp=W_pinv)
                logger.update(eW=W_m)
                # alpha, e


            model.train()
            opt.zero_grad()
            total_loss = 0
            total_loss_c = 0
            total_loss_d = 0
            L = 0
            for batch in loader:
                x, y = batch

                y_one_hot = torch.nn.functional.one_hot(y, num_classes=n_classes) # B*n
                pred = base(x)

                wsum = torch.sum(pred.view(-1,n_outs,1)*y_one_hot.view(-1,1,n_classes), dim=0) # N*n
                W = wsum/counts

                loss_c = torch.sum(W*W_m_t)*K
                loss_d = -torch.sum(W*W_pinv_T)*K
                loss = loss_d + alpha * loss_c
                total_loss_c += loss_c.item()
                total_loss_d += loss_d.item()
                total_loss += loss.item()
                loss.backward()

                # Lipschitz part of the loss

                if True:
                    clone.zero_grad()
                    x_ = clone[0](x)
                    x_.requires_grad = True
                    fx = clone[1:](x_)
                    L_loss = torch.sum(fx)
                    L_loss.backward()
                    dx = x_.grad
                    L = max(L, torch.max(dx.norm(dim=1)).item())

                    if target_L is not None:
                        ext_dx = dx[None,...] * dx_ # 2*B*n_features
                        ext_x = x_[None,...] + ext_dx

                        pred_dfx = meat(ext_x) # 2*B*N
                        dfx = (pred_dfx[1] - pred_dfx[0])/(2*dxl)/target_L
                        loss_L = torch.sum(F.relu(torch.abs(dfx)-1)**2) / len(loader.dataset)

                        total_loss += loss_L.item()
                        loss_L.backward()

            opt.step()
            log.info("Epoch %d loss=%f W+c=%s logdet=%f L=%f",
                    epoch, total_loss, W_pinv[selected_class], logdet, L)
            if logger is not None:
                logger.log(
                    total_loss=total_loss,
                    loss_c=total_loss_c,
                    loss_d=total_loss_d,
                    L=L,
                )
    except KeyboardInterrupt:
        pass
    model.eval()
    model.zero_grad(set_to_none=True)
    fix_linear(model, loader, n_outs, n_classes, selected_class, e_, bounds, device=device)
    return model



def train_weighting(model, dataset, n_outs, n_classes, selected_class, e_=0.02, bounds=BOUNDS, epochs=1000, device=None, logger=None, **kwargs):
    model.to(device)
    data = torch.utils.data.TensorDataset(
        dataset.X(device=device, dtype=torch.float),
        dataset.Y(device=device, dtype=torch.int64)
    )
    loader = Loader(data, batch_size=100000, shuffle=False, device=device)
    train_pinv(model, loader, n_outs, n_classes, selected_class, e_=e_, bounds=bounds, epochs=50, alpha=1, logger=logger, device=device)
    train_pinv(model, loader, n_outs, n_classes, selected_class, e_=e_, bounds=bounds, epochs=epochs, alpha=300, logger=logger, device=device, **kwargs)
    #fix_linear(model, loader, n_outs, n_classes, selected_class, e_, bounds, device=device)
    return model.eval()


def train_estimator(dataset, model, device=None, logger=None, val_dataset=None, batch_size=100000, batch_epochs=50, full_epochs=50):
    model.to(device)
    ds = torch.utils.data.TensorDataset(
        dataset.X(device=device, dtype=float),
        dataset.Y(device=device, dtype=torch.int64)
    )
    val_loader = None
    if val_dataset is not None:
        vds = torch.utils.data.TensorDataset(
            val_dataset.X(device=device, dtype=float),
            val_dataset.Y(device=device, dtype=torch.int64)
        )
        val_loader = Loader(vds, batch_size=batch_size, shuffle=False, device=device)
    loader = Loader(ds, batch_size=batch_size, shuffle=True, device=device)
    train(model, loader, epochs=batch_epochs, weight=dataset.weights, accumulate=False, logger=logger, val_loader=val_loader, device=device)
    loader = Loader(ds, batch_size=batch_size, shuffle=False, device=device)
    train(model, loader, epochs=full_epochs, weight=dataset.weights, accumulate=True, logger=logger, val_loader=val_loader, device=device)
    return model.eval()




def validate(model, loader, wt):
    model.eval()
    total_loss = 0
    total_count = 0
    for batch in loader:
        x, y = batch
        pred = model(x)
        loss = F.cross_entropy(
            input=pred,
            target=y,
            weight=wt,
            reduction='sum',
        )
        total_loss += loss.item()
        total_count += x.shape[0]
    return total_loss / total_count



def train(model, loader, epochs=20, weight=None, accumulate=False, logger=None, val_loader=None, device=None):
    total_count = len(loader.dataset)
    model.train()
    model.zero_grad()
    opt = torch.optim.Adam(params=model.parameters())
    opt.zero_grad()
    wt = util.from_numpy(weight, device=device) if weight is not None else None

    if logger is not None:
        train_loss = validate(model, loader, wt)
        logger.update(model=util.clone_model(model).cpu())
        logger.update(train_loss=train_loss)
        if val_loader is not None:
            val_loss = validate(model, val_loader, wt)
            logger.update(val_loss=val_loss)

    for epoch in range(epochs):
        model.train()

        for batch in loader:
            x, y = batch
            pred = model(x)
            loss = F.cross_entropy(
                input=pred,
                target=y,
                weight=wt,
                reduction='sum',
            )
            count = x.shape[0] if not accumulate else total_count
            loss /= count
            loss.backward()
            if not accumulate:
                opt.step()
                opt.zero_grad()
        if accumulate:
            opt.step()
            opt.zero_grad()

        train_loss = validate(model, loader, wt)
        val_loss = validate(model, val_loader, wt) if val_loader is not None else None

        if val_loss is None:
            log.debug("Epoch %d, train=%f", epoch, train_loss)
        else:
            log.debug("Epoch %d, train=%f val=%f", epoch, train_loss, val_loss)

        if logger is not None:
            logger.log(train_loss=train_loss, val_loss=val_loss)
            logger.update(model=util.clone_model(model).cpu())
    model.eval()
    model.zero_grad(set_to_none=True)
    return model
