import numpy as np
import torch
import sklearn.model_selection
import uproot
uproot.default_library = "np"

import logging

log = logging.getLogger(__name__)

class Dataset(object):
    def __init__(self, features, sources, aliases={}, name=None):
        self.sources = sources
        self.features = features
        self.name = name
        self.counts = np.asarray([len(source) for source in self.sources])
        self.classes = [source.name for source in self.sources]
        self.size = np.sum(self.counts)
        self.weights = self.size/self.counts/self.counts.size
        self.aliases = aliases
        log.info("Dataset counts: %s", self.counts)

    def __len__(self):
        return np.sum(self.counts)

    def __repr__(self):
        return "DS%s" % self.name

    def X(self, **kwargs):
        ccounts = np.cumsum(self.counts)
        lower = ccounts - self.counts
        result = torch.empty(ccounts[-1], len(self.features), **kwargs)
        for i in range(ccounts.size):
            np_data = self.sources[i].get(self.features, aliases=self.aliases)
            result[lower[i]:ccounts[i]] = torch.as_tensor(np_data, **kwargs)
        return result

    def Y(self, **kwargs):
        ccounts = np.cumsum(self.counts)
        result = torch.zeros(ccounts[-1], **kwargs)
        for i in range(1, ccounts.size):
            result[ccounts[i-1]:ccounts[i]] = i
        return result

    def Y_hot(self, **kwargs):
        ccounts = np.cumsum(self.counts)
        result = torch.zeros(ccounts[-1], ccounts.size, **kwargs)
        for i in range(1, ccounts.size):
            result[ccounts[i-1]:ccounts[i], i] = 0
        return result

class PlainDataset:
    def __init__(self, features, classes, X, Y, name=None):
        self.features = features
        self.classes = classes
        self.sources = classes
        self.name = name
        assert len(X.shape) == 2
        assert len(Y.shape) == 1
        assert X.shape[0] == Y.shape[0]
        self.X_ = X.detach()
        self.Y_ = Y.detach()
        self.counts = torch.sum(torch.nn.functional.one_hot(self.Y_, len(classes)), dim=0).cpu().numpy()
        self.size = Y.shape[0]
        self.weights = self.size/self.counts/self.counts.size
        log.info("Dataset counts: %s", self.counts)

    def __len__(self):
        return self.size()

    def __repr__(self):
        return "DS%s" % self.name

    def X(self, **kwargs):
        return torch.as_tensor(self.X_, **kwargs)

    def Y(self, **kwargs):
        return torch.as_tensor(self.Y_, **kwargs)

    def Y_hot(self, **kwargs):
        Y = self.Y(**kwargs)
        return torch.nn.functional.one_hot(Y, len(self.classes))


class DataFile(object):
    def __init__(self, path, name, aliases={}):
        self.path = path
        self.name = name
        self.aliases = aliases
        self.raw = uproot.open(self.path)
        log.info("Open '%s' as '%s' with %d entries", path, name, len(self))

    def features(self):
        return sorted(list(set(list(self.raw.keys()) + list(self.aliases.keys()))))

    def true_features(self):
        return [name for name in self.features() if "TRUE" in name]

    def __len__(self):
        return self.raw.num_entries

    def __item__(self, name):
        return self.raw[name].array(library="np")

    def get(self, features, aliases={}):
        res = self.raw.arrays(features, aliases=(self.aliases|aliases), library="np")
        array = np.empty((self.raw.num_entries, len(features)), dtype=float)
        for i, feature in enumerate(features):
            array[:,i] = res[feature]
        return array

class SelectedData(object):
    def __init__(self, datafile, selector):
        assert selector.shape == (len(datafile), )
        self.datafile = datafile
        self.index = np.asarray(selector, dtype=bool)
        self.size = np.sum(self.index)
        self.name = self.datafile.name

    def get(self, features, aliases={}):
        return self.datafile.get(features, aliases=aliases)[self.index]

    def true_features(self):
        return self.datafile.true_features()

    def features(self):
        return self.datafile.features()

    def __len__(self):
        return self.size

def split(selected, **options):
    selector = selected.index
    arr = np.arange(len(selector))[selector]
    train_index, test_index = sklearn.model_selection.train_test_split(arr, **options)
    train_selector = np.zeros_like(selector, dtype=bool)
    test_selector = np.zeros_like(selector, dtype=bool)
    train_selector[train_index] = True
    test_selector[test_index] = True
    train = SelectedData(selected.datafile, train_selector)
    test = SelectedData(selected.datafile, test_selector)
    return train, test




def join_and(*conditions):
    return "&".join(["(%s)"%condition for condition in conditions])

def q2_range(v1, v2, true=True):
    feature = "q2_TRUE" if true else "q2"
    return join_and("%s > %g" % (feature, v1), "%s < %g" % (feature, v2))

def q_range(v1, v2, true=True):
    return q2_range(v1**2, v2**2, true=true)

def e_selectors(true=True, q=None, qw=0.25):
    q_Kll_selector = \
            q2_range(1.1, 6.0, true) \
            if q is None else \
            q_range(q-qw, q+qw, true)
    q_Jpsi_e_selector = q2_range(6.0, 12.96, true)
    q_psi2S_e_selector = q2_range(9.82, 16.40, true)
    selectors = [
        q_Kll_selector,
        q_Jpsi_e_selector,
        q_psi2S_e_selector,
        "True",
    ]
    return selectors

def truthing(files, true=True, q=None, qw=0.25):
    selectors = e_selectors(true, q, qw)
    return [SelectedData(f, f.get([selector])[:,0]) for f, selector in zip(files, selectors)]

