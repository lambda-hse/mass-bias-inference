#!/usr/bin/env python

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation

import numpy as np
import pandas as pd

import pickle

import argparse
import logging

import lib.graph_util
import lib.graph
import lib.util
import lib.tqlog


log = logging.getLogger(__name__)

np.set_printoptions(precision=4, suppress=True)



def draw_w(w, ej=0.02, lim=0.2, ax=None):
    axes = plt.gca() if ax is None else ax
    wp = np.linalg.pinv(w)
    alpha = lib.trainer.make_weighting_lp(wp)
    e = w.T@alpha
    ek = e[0]

    wcj = wp[0]
    wcj_ = (wcj > 0).astype(np.float64)
    ek_ = ej + np.min((wcj_-ej)/wcj)

    print("e=", e)
    print("α=", alpha)
    print("W", w)
    print("W+", wp)

    mx = np.maximum(-lim/2+ek, 0)

    def line(dx, dy, c):
        # dx*x+dy*y=c
        x0 = (c/dx)/2
        y0 = (c/dy)/2
        x1 = x0 - dy
        x2 = x0 + dy
        y1 = y0 + dx
        y2 = y0 - dx
        res = lib.graph_util.liang_barsky_clipper(mx, 0, mx+lim, lim, x1, y1, x2, y2)
        if res is not None:
            axes.axline(*res)

    for col in wp.T:
        k, j, p, *other = col
        j_ = sum(other, j)
        line(k, p, 0-ej*j_)
        line(k, p, 1-ej*j_)


    axes.scatter([ek, ek_], [e[2], ej], c='black')
    axes.set_xlim(mx,mx+lim)
    axes.set_ylim(0,lim)
    axes.axvline(ek_, c='y')
    axes.axvline(ek, c='r')
    axes.axhline(0.01, c='g')
    axes.axhline(0.03, c='g')
    axes.grid()
    axes.set_aspect("equal")
    axes.set_xlabel("e'(K)")
    axes.set_ylabel("e'(ψ(2S))")
    if ax is None:
        plt.show()


def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    parser.add_argument('--dpi', dest='dpi', default=120, type=int, required=False)
    parser.add_argument('--fps', dest='fps', default=20, type=int, required=False)
    parser.add_argument('--after', dest='after', default=50, type=int, required=False)
    args = parser.parse_args()
    return args


def main():

    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[lib.tqlog.TqdmLoggingHandler()])

    with open(args.input, "rb") as f:
        w_history = pickle.load(f)

    matplotlib.use('agg')
    plt.ioff()

    writer = matplotlib.animation.FFMpegWriter(fps=args.fps, extra_args=["-loglevel", "error", "-hide_banner"])
    fig, ((ax2, ax3), (ax1, ax4)) = plt.subplots(2, 2, figsize=(10, 6), facecolor='white')
    ax3.plot(*w_history.extract("rk"), label="RK")
    ax3.plot(*w_history.extract("rk0"), label="RK0")
    ax3.grid()
    ax3.legend()
    ax4.plot(*w_history.extract("logdet"), label="log det")
    ax4.grid()
    ax4.legend()
    rk_line = ax3.axvline(c='r')
    ld_line = ax4.axvline(c='r')
    with writer.saving(fig, args.output, dpi=args.dpi):
        for i, m in enumerate(w_history.measurements):
            try:
                log.info("Frame %d", i)
                w = m.get("W", None)
                if w is None:
                    continue
                rk_line.set_xdata(i)
                ld_line.set_xdata(i)
                ax1.clear()
                ax2.clear()
                draw_w(w, lim=0.04, ax=ax1)
                draw_w(w, lim=0.5, ax=ax2)
                writer.grab_frame()
            except KeyboardInterrupt:
                break
        for i in range(args.after):
            writer.grab_frame()


if __name__ == '__main__':
    main()
