#!/usr/bin/env python3

import numpy as np
import pandas as pd

import pickle

import argparse
import logging

import lib.util
import lib.setup
import lib.files


log = logging.getLogger(__name__)
np.set_printoptions(precision=4, suppress=True)


def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.INFO, type=str)
    parser.add_argument('--dataset', dest='dataset', default=None, type=str, required=True)
    parser.add_argument('input', type=str, nargs='+')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)


    sources = lib.files.load(args.dataset)
    truthed = lib.data.truthing(sources, true=True)
    aliases = {
        "min_e_IP_TRUE": "fmin(ep_IP_TRUE,em_IP_TRUE)",
        "max_e_PT_TRUE": "fmax(ep_PT_TRUE,em_PT_TRUE)",
        "max_e_SIGMAIP_TRUE": "fmax(ep_SIGMAIP_TRUE,em_SIGMAIP_TRUE)",
        "max_e_P_TRUE": "fmax(ep_P_TRUE,em_P_TRUE)",

        "min_e_IP": "fmin(ep_IP,em_IP)",
        "max_e_PT": "fmax(ep_PT,em_PT)",
        "max_e_P": "fmax(ep_P,em_P)",
    }


    table = list()
    for name in args.input:
        with open(name, "rb") as f:
            w_history = pickle.load(f)

        rks = w_history.extract("rk")
        best = np.argmax(rks[1])
        frame = rks[0][best]
        log.info("Frame %d", frame)

        m = w_history.measurements[frame]
        model = m.get("model", None)
        f = lib.util.as_numpy(model)
        features = model[0].features

        rec = {'name': name}

        for values in truthed:
            data = lib.data.Dataset(features, [values], aliases)
            x = data.X()
            w = f(x)
            rec[values.name] = (1-np.mean(w))/0.98

        table.append(rec)

    df = pd.DataFrame.from_records(table)
    print(df.to_markdown())


if __name__ == '__main__':
    main()
