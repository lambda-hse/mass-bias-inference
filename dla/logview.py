#!/usr/bin/env python

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation

import numpy as np
import pandas as pd

import pickle

import argparse
import logging

import lib.graph_util
import lib.graph
import lib.util
import lib.tqlog


log = logging.getLogger(__name__)

np.set_printoptions(precision=4, suppress=True)


def draw_wp(wp, ej=0.02, lim=0.2, ax=None):
    axes = plt.gca() if ax is None else ax

    def line(dx, dy, c):
        # dx*x+dy*y=c
        x0 = (c/dx)/2
        y0 = (c/dy)/2
        x1 = x0 - dy
        x2 = x0 + dy
        y1 = y0 + dx
        y2 = y0 - dx
        res = lib.graph_util.liang_barsky_clipper(0, 0, lim, lim, x1, y1, x2, y2)
        if res is not None:
            axes.axline(*res)

    for col in wp.T:
        k, j, p, *other = col
        j_ = sum(other, j)
        line(k, p, 0-ej*j_)
        line(k, p, 1-ej*j_)

    axes.set_xlim(0,lim)
    axes.set_ylim(0,lim)
    axes.grid()
    axes.set_aspect("equal")
    axes.set_xlabel("e'(K)")
    axes.set_ylabel("e'(psi2S)")
    if ax is None:
        plt.show()



def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    parser.add_argument('--dpi', dest='dpi', default=120, type=int, required=False)
    parser.add_argument('--fps', dest='fps', default=20, type=int, required=False)
    parser.add_argument('--after', dest='after', default=50, type=int, required=False)
    args = parser.parse_args()
    return args


def main():

    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[lib.tqlog.TqdmLoggingHandler()])

    with open(args.input, "rb") as f:
        w_history = pickle.load(f)

    matplotlib.use('agg')
    plt.ioff()

    writer = matplotlib.animation.FFMpegWriter(fps=args.fps, extra_args=["-loglevel", "error", "-hide_banner"])
    fig, ((ax2, ax3), (ax1, ax4)) = plt.subplots(2, 2, figsize=(10, 6), facecolor='white')
    ax3.plot(*w_history.extract("rk"), label="RK")
    ax3.plot(*w_history.extract("rk0"), label="RK0")
    ax3.grid()
    ax3.legend()
    ax4.plot(*w_history.extract("logdet"), label="log det")
    ax4.grid()
    ax4.legend()
    rk_line = ax3.axvline(c='r')
    ld_line = ax4.axvline(c='r')
    with writer.saving(fig, args.output, dpi=args.dpi):
        t, wps = w_history.extract("Wp")
        for i, m in enumerate(w_history.measurements):
            try:
                log.info("Frame %d", i)
                wp = m.get("Wp", None)
                if wp is None:
                    continue
                model = m.get("model", None)
                if model is None:
                    continue
                sc = model[0]
                f = lib.util.as_numpy(model)
                rk_line.set_xdata(i)
                ld_line.set_xdata(i)
                ax1.clear()
                ax2.clear()
                lib.graph.plot_field(f, sc, ax=ax2, color_bar=False)
                draw_wp(wp, lim=0.50, ax=ax1)
                writer.grab_frame()
            except KeyboardInterrupt:
                break
        for i in range(args.after):
            writer.grab_frame()


if __name__ == '__main__':
    main()
