#!/usr/bin/env python3

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation

import numpy as np
import pandas as pd

import pickle

import argparse
import logging

import lib.graph_util
import lib.graph
import lib.util
import lib.setup


log = logging.getLogger(__name__)
np.set_printoptions(precision=4, suppress=True)


def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--frame', dest='frame', default=None, type=int, required=False)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)

    with open(args.input, "rb") as f:
        w_history = pickle.load(f)

    matplotlib.use('agg')
    plt.ioff()

    rks = w_history.extract("rk")
    rk0s = w_history.extract("rk0")

    frame = args.frame
    if frame is None:
        best = np.argmax(rks[1])
        frame = rks[0][best]
    log.info("Frame %d", frame)

    i = frame
    m = w_history.measurements[frame]
    w = m["W"]

    wp = np.linalg.pinv(w)
    alpha = lib.trainer.make_weighting_lp(wp)
    e = w.T@alpha
    ek = e[0]

    print(alpha)
    print(e)
    print(m)


if __name__ == '__main__':
    main()
