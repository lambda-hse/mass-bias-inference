#!/usr/bin/env python3

import numpy as np
import pandas as pd

import argparse
import logging

import lib.graph_util
import lib.graph
import lib.util
import lib.setup


log = logging.getLogger(__name__)
np.set_printoptions(precision=4, suppress=True)


def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--batch', dest='batch', default=None, type=int, required=True)
    parser.add_argument('--samples', dest='samples', default=None, type=int, required=True)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)

    with np.load(args.input, "rb") as f:
        df = pd.DataFrame(columns=["size", "mean", "std", "b-mean", "b-std"], index=f.keys())
        for k in df.index:
            arr = 1-f[k][:,0]
            res = [np.mean(np.random.choice(arr, args.batch)) for _ in range(args.samples)]
            df["size"][k] = arr.size
            df["mean"][k] = np.mean(arr)
            df["std"][k] = np.std(arr)
            df["b-mean"][k] = np.mean(res)
            df["b-std"][k] = np.std(res)

        print(df)

if __name__ == '__main__':
    main()
