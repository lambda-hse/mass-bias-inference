#!/usr/bin/env python3

import torch
import numpy as np

import argparse
import logging

import pickle

import lib.tqlog
import lib.model
import lib.data
import lib.files
import lib.trainer
import lib.logger


log = logging.getLogger(__name__)



def parse_args():
    default_device = "cuda:0" if torch.cuda.is_available() else "cpu"
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--device', dest='device', help='GPU device id to use [%s]' % default_device, default=default_device, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[lib.tqlog.TqdmLoggingHandler()])
    log.info("device: %s", args.device)

    np.set_printoptions(precision=4, suppress=True)

    data_dict = torch.load(args.input)

    data = lib.data.PlainDataset(
            data_dict['features'],
            data_dict['classes'],
            data_dict['X'],
            data_dict['Y'],
            )
    model = lib.model.make_estimator(data, device=args.device)

    logger = lib.logger.Logger()

    lib.trainer.train_estimator(data, model, device=args.device, logger=logger)

    with open(args.output, "wb") as f:
        log.info("Saving log to '%s'", args.output)
        pickle.dump(logger, f)


if __name__ == '__main__':
    main()
