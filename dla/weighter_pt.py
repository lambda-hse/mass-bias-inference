#!/usr/bin/env python3

import torch
import numpy as np

import argparse
import logging

import pickle

import lib.tqlog
import lib.model
import lib.data
import lib.files
import lib.setup



log = logging.getLogger(__name__)


def parse_args():
    default_device = "cuda:0" if torch.cuda.is_available() else "cpu"
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--device', dest='device', help='GPU device id to use [%s]' % default_device, default=default_device, type=str)
    parser.add_argument('--data', dest='data', default=None, type=str, required=True)
    parser.add_argument('--epochs', dest='epochs', default=1000, type=int, required=False)
    parser.add_argument('--state', dest='state', default=None, type=str, required=True)
    parser.add_argument('--output', dest='output', default=None, type=str, required=True)
    parser.add_argument('--boundaries', dest='bounds', default=None, type=str, required=False)
    parser.add_argument('--effdrop', dest='e_', default=None, type=float, required=False)
    parser.add_argument('--gp', dest='gp', default=None, type=float, required=False)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)

    log.info("device: %s", args.device)

    np.set_printoptions(precision=4, suppress=True)

    data_dict = torch.load(args.data)
    data = lib.data.PlainDataset(
            data_dict['features'],
            data_dict['classes'],
            data_dict['X'],
            data_dict['Y'],
            )

    with open(args.state, "rb") as f:
        log.info("Loading state from '%s'", args.state)
        history = pickle.load(f)

    model = history.last()["model"]
    features = model[0].features
    log.info("features: %s", features)
    assert features == data_dict["features"]

    logger = lib.logger.Logger()

    e_ = 0.03
    if args.bounds is not None:
        b = [float(x) for x in args.bounds.split(",")]
    else:
        b = [0.03]

    bounds = [(max(0, 1-(1+x)*(1-e_)), max(0, 1-(1-x)*(1-e_))) for x in b]
    log.info("Bounds: %s, E: %f", bounds, e_)

    w_model = lib.model.w_from_est(model)
    w_model.train(data,
            logger=logger,
            device=args.device,
            epochs=args.epochs,
            target_L=args.gp,
            e_=e_,
            bounds=bounds,
            )

    with open(args.output, "wb") as f:
        log.info("Saving log to '%s'", args.output)
        pickle.dump(logger, f)


if __name__ == '__main__':
    main()
