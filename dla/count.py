#!/usr/bin/env python3

import numpy as np
import pandas as pd

import pickle

import argparse
import logging

import lib.util
import lib.setup
import lib.files


log = logging.getLogger(__name__)
np.set_printoptions(precision=4, suppress=True)


def parse_args():
    parser = argparse.ArgumentParser(description='Log viewer')
    parser.add_argument('--log', dest='log', default=None, type=str)
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--dataset', dest='dataset', default=None, type=str, required=True)
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    lib.setup.logging(level=args.log_level, dest=args.log)

    sources = lib.files.load(args.dataset)
    truthed = lib.data.truthing(sources, true=True)

    result = list()
    for source in truthed:
        # DataFile
        name = source.name
        selected = len(source)
        total = len(source.datafile)
        result.append((name, total, selected))

    df = pd.DataFrame.from_records(result)
    print(df.to_string())



if __name__ == '__main__':
    main()

