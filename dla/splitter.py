#!/usr/bin/env python3

import torch
import numpy as np
import sklearn.model_selection

import argparse
import logging

import pickle

import lib.tqlog
import lib.model
import lib.data
import lib.files
import lib.trainer
import lib.logger


log = logging.getLogger(__name__)



def parse_args():
    default_device = "cuda:0" if torch.cuda.is_available() else "cpu"
    parser = argparse.ArgumentParser(description='train.py')
    parser.add_argument('--log-level', dest='log_level', default=logging.NOTSET, type=str)
    parser.add_argument('--device', dest='device', help='GPU device id to use [%s]' % default_device, default=default_device, type=str)
    parser.add_argument('--input', dest='input', default=None, type=str, required=True)
    parser.add_argument('--test', dest='test', default=None, type=str, required=True)
    parser.add_argument('--train', dest='train', default=None, type=str, required=True)
    args = parser.parse_args()
    return args

def save(name, data):
    log.info("Saving data to '%s'", name)
    meta = dict()
    meta["features"] = data.features
    meta["classes"] = data.classes
    meta["name"] = data.name
    meta["X"] = data.X(dtype=torch.float)
    meta["Y"] = data.Y(dtype=torch.int64)
    torch.save(meta, name)

def main():
    args = parse_args()
    logging.basicConfig(level=args.log_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', force=True, handlers=[lib.tqlog.TqdmLoggingHandler()])
    log.info("device: %s", args.device)

    np.set_printoptions(precision=4, suppress=True)

    data_dict = torch.load(args.input)

    features = data_dict['features']
    classes = data_dict['classes']
    name = data_dict['name']

    X = data_dict['X']
    Y = data_dict['Y']

    arr = np.arange(Y.shape[0])
    train_ix, test_ix = sklearn.model_selection.train_test_split(arr)

    train = lib.data.PlainDataset(
            features,
            classes,
            X[train_ix],
            Y[train_ix],
            "%s %s" % (name, 'train'),
            )
    test = lib.data.PlainDataset(
            features,
            classes,
            X[test_ix],
            Y[test_ix],
            "%s %s" % (name, 'test'),
            )


    save(args.train, train)
    save(args.test, test)



if __name__ == '__main__':
    main()
